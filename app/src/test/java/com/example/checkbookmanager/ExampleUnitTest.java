package com.example.checkbookmanager;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void getsCorrectCurrencies() throws Exception {
        CurrencyConversionActivity activity = new CurrencyConversionActivity();
        activity.getCurrencies("USD");
        List<String> realCurrencies = new LinkedList<>();
        realCurrencies.add("AUD");
        realCurrencies.add("BGN");
        realCurrencies.add("BRL");
        realCurrencies.add("CAD");
        realCurrencies.add("EUR");
        realCurrencies.add("CHF");
        realCurrencies.add("CNY");
        realCurrencies.add("CZK");
        realCurrencies.add("DKK");
        realCurrencies.add("GBP");
        realCurrencies.add("HKD");
        realCurrencies.add("HRK");
        realCurrencies.add("HUF");
        realCurrencies.add("IDR");
        realCurrencies.add("ILS");
        realCurrencies.add("INR");
        realCurrencies.add("JPY");
        realCurrencies.add("KRW");
        realCurrencies.add("MXN");
        realCurrencies.add("MYR");
        realCurrencies.add("NOK");
        realCurrencies.add("NZD");
        realCurrencies.add("PHP");
        realCurrencies.add("PLN");
        realCurrencies.add("RON");
        realCurrencies.add("RUB");
        realCurrencies.add("SEK");
        realCurrencies.add("SGD");
        realCurrencies.add("THB");
        realCurrencies.add("TRY");
        realCurrencies.add("USD");
        realCurrencies.add("ZAR");
        List<String> currencies = activity.getCurrencies();
        for(String currency: realCurrencies){
            assertTrue(currencies.contains(currency));
        }
    }


    @Test
    public void accountConversionConvertingRightAmount () throws Exception {
        CurrencyConversionActivity activity = new CurrencyConversionActivity();
        activity.getCurrencies("USD");
        Account account = new Account("test","test",0L,0L,10.0);
        Map<String, Double> rates = activity.getRates();
        for(Map.Entry<String, Double> entry: rates.entrySet()){
            double initialBalance = account.balance;
            account.setCurrency(entry.getKey(),entry.getValue());
            assertEquals(initialBalance*entry.getValue(),account.getBalance(),0.001);
        }
    }

    @Test
    public void gettingActiveAndroidToWork() throws Exception{
        WelcomeActivity activity = new WelcomeActivity();
        ActiveAndroid.initialize(activity.getApplication());
        Account account = new Account("test","test",0L,0L,10.0);
        Transaction transaction = new Transaction(true, 10.0, "paycheck",new GregorianCalendar(),"notes");
        account.save();
        transaction.save();
        Transaction transaction2 = Transaction.load(Transaction.class,transaction.getId());
        Account account2 = Account.load(Account.class,transaction.getId());
        assertEquals(transaction,transaction2);
        assertEquals(account,account2);
    }


}