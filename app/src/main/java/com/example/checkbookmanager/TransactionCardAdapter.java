package com.example.checkbookmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class TransactionCardAdapter extends RecyclerView.Adapter<TransactionCardAdapter.ViewHolder> {
    public List<Transaction> mDataset;
    private AccountActivity activity;



    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        // each data item is just a string in this case
        private TextView mAmount;
        private TextView mDestSource;
        private CardView mCard;
        private TextView mTransactionID;
        private Transaction transaction;
        private AccountActivity activity;
        public ViewHolder(View v, AccountActivity activity) {
            super(v);
            mCard=(CardView) v.findViewById(R.id.transaction_card_view);
            mAmount = (TextView) v.findViewById(R.id.transactionAmount);
            mDestSource = (TextView) v.findViewById(R.id.transactionDestSource);
            mTransactionID = (TextView) v.findViewById(R.id.transactionID);
            mCard.setClickable(true);
            mCard.setOnClickListener(this);
            this.activity = activity;
        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            Intent transActionIntent = new Intent(context, EditTransactionActivity.class);
            transActionIntent.putExtra("transID", Integer.parseInt(mTransactionID.getText().toString()));
            transActionIntent.putExtra("transaction", transaction);
            activity.addTransaction(v, transActionIntent);
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TransactionCardAdapter(List<Transaction> myDataset, AccountActivity activity) {
        mDataset = myDataset;
        this.activity = activity;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public TransactionCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_view_transaction_row, parent, false);
//        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(inflatedView, activity);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.transaction = mDataset.get(position);
        String amountString;
        if (holder.transaction.isDebit()){
            amountString=""+holder.transaction.getAmount();
            holder.mAmount.setTextColor(Color.GREEN);
        } else {
            amountString="-"+holder.transaction.getAmount();
            holder.mAmount.setTextColor(Color.RED);
        }
        holder.mAmount.setText(amountString);
        holder.mDestSource.setText(holder.transaction.getSourceFrom());
        holder.mTransactionID.setText(""+holder.transaction.getTransactionId());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}