package com.example.checkbookmanager;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class EditAccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        Intent intent = getIntent();
    }

    public void saveAccount(View view){
        Intent data = new Intent();
        EditText nameEditText = (EditText) findViewById(R.id.name);
        EditText bankEditText = (EditText) findViewById(R.id.bank);
        EditText accountNumberEditText = (EditText) findViewById(R.id.accountNumber);
        EditText routingNumberEditText = (EditText) findViewById(R.id.routingNumber);
        EditText balanceEditText = (EditText) findViewById(R.id.balance);
        if(errorChecking()) {
            String name = nameEditText.getText().toString();
            String bank = bankEditText.getText().toString();
            long accountNumber = Long.parseLong(accountNumberEditText.getText().toString());
            long routingNumber = Long.parseLong(routingNumberEditText.getText().toString());
            double balance = Double.parseDouble(balanceEditText.getText().toString());
            Account account = new Account(name, bank, accountNumber, routingNumber, balance);
            data.putExtra("account", account);
            setResult(RESULT_OK, data);
            finish();
        } else {
            ErrorDialog errorDialog = new ErrorDialog();
            errorDialog.show(this.getFragmentManager(),"");
        }
    }

    private boolean errorChecking(){
        EditText nameEditText = (EditText) findViewById(R.id.name);
        EditText bankEditText = (EditText) findViewById(R.id.bank);
        EditText accountNumberEditText = (EditText) findViewById(R.id.accountNumber);
        EditText routingNumberEditText = (EditText) findViewById(R.id.routingNumber);
        EditText balanceEditText = (EditText) findViewById(R.id.balance);
        if (nameEditText.getText().toString().equals("")||(bankEditText.getText().toString().equals(""))||
                (accountNumberEditText.getText().toString().equals(""))||(routingNumberEditText.getText().toString().equals(""))||
                (balanceEditText.getText().toString().equals(""))){
            return false;
        }
        for(Account acc: WelcomeActivity.accountList){
            if(acc.getAccountName().equals(nameEditText.getText().toString())){
                return false;
            }
        }
        return true;
    }


}
