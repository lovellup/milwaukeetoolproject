package com.example.checkbookmanager;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Rate {

    private Double AUD;
    private Double BGN;
    private Double BRL;
    private Double CAD;
    private Double EUR;
    private Double CHF;
    private Double CNY;
    private Double CZK;
    private Double DKK;
    private Double GBP;
    private Double HKD;
    private Double HRK;
    private Double HUF;
    private Double IDR;
    private Double ILS;
    private Double INR;
    private Double JPY;
    private Double KRW;
    private Double MXN;
    private Double MYR;
    private Double NOK;
    private Double NZD;
    private Double PHP;
    private Double PLN;
    private Double RON;
    private Double RUB;
    private Double SEK;
    private Double SGD;
    private Double THB;
    private Double TRY;
    private Double USD;
    private Double ZAR;

    public Double getAUD() {
        return AUD;
    }

    public void setAUD(Double aUD) {
        this.AUD = aUD;
    }

    public Double getBGN() {
        return BGN;
    }

    public void setBGN(Double bGN) {
        this.BGN = bGN;
    }

    public Double getBRL() {
        return BRL;
    }

    public void setBRL(Double bRL) {
        this.BRL = bRL;
    }

    public Double getCAD() {
        return CAD;
    }

    public void setCAD(Double cAD) {
        this.CAD = cAD;
    }

    public Double getCHF() {
        return CHF;
    }

    public void setCHF(Double cHF) {
        this.CHF = cHF;
    }

    public Double getCNY() {
        return CNY;
    }

    public void setCNY(Double cNY) {
        this.CNY = cNY;
    }

    public Double getCZK() {
        return CZK;
    }

    public void setCZK(Double cZK) {
        this.CZK = cZK;
    }

    public Double getDKK() {
        return DKK;
    }

    public void setDKK(Double dKK) {
        this.DKK = dKK;
    }

    public Double getGBP() {
        return GBP;
    }


    public void setGBP(Double gBP) {
        this.GBP = gBP;
    }

    public Double getHKD() {
        return HKD;
    }

    public void setHKD(Double hKD) {
        this.HKD = hKD;
    }

    public Double getHRK() {
        return HRK;
    }

    public void setHRK(Double hRK) {
        this.HRK = hRK;
    }

    public Double getHUF() {
        return HUF;
    }

    public void setHUF(Double hUF) {
        this.HUF = hUF;
    }

    public Double getIDR() {
        return IDR;
    }

    public void setIDR(Double iDR) {
        this.IDR = iDR;
    }

    public Double getILS() {
        return ILS;
    }

    public void setILS(Double iLS) {
        this.ILS = iLS;
    }

    public Double getINR() {
        return INR;
    }

    public void setINR(Double iNR) {
        this.INR = iNR;
    }

    public Double getJPY() {
        return JPY;
    }

    public void setJPY(Double jPY) {
        this.JPY = jPY;
    }

    public Double getKRW() {
        return KRW;
    }

    public void setKRW(Double kRW) {
        this.KRW = kRW;
    }

    public Double getMXN() {
        return MXN;
    }

    public void setMXN(Double mXN) {
        this.MXN = mXN;
    }

    public Double getMYR() {
        return MYR;
    }

    public void setMYR(Double mYR) {
        this.MYR = mYR;
    }

    public Double getNOK() {
        return NOK;
    }

    public void setNOK(Double nOK) {
        this.NOK = nOK;
    }

    public Double getNZD() {
        return NZD;
    }

    public void setNZD(Double nZD) {
        this.NZD = nZD;
    }

    public Double getPHP() {
        return PHP;
    }

    public void setPHP(Double pHP) {
        this.PHP = pHP;
    }

    public Double getPLN() {
        return PLN;
    }

    public void setPLN(Double pLN) {
        this.PLN = pLN;
    }

    public Double getRON() {
        return RON;
    }

    public void setRON(Double rON) {
        this.RON = rON;
    }

    public Double getRUB() {
        return RUB;
    }

    public void setRUB(Double rUB) {
        this.RUB = rUB;
    }

    public Double getSEK() {
        return SEK;
    }

    public void setSEK(Double sEK) {
        this.SEK = sEK;
    }

    public Double getSGD() {
        return SGD;
    }

    public void setSGD(Double sGD) {
        this.SGD = sGD;
    }

    public Double getTHB() {
        return THB;
    }

    public void setTHB(Double tHB) {
        this.THB = tHB;
    }

    public Double getTRY() {
        return TRY;
    }

    public void setTRY(Double tRY) {
        this.TRY = tRY;
    }

    public Double getUSD() {
        return USD;
    }

    public void setUSD(Double uSD) {
        this.USD = uSD;
    }

    public Double getZAR() {
        return ZAR;
    }

    public void setZAR(Double zAR) {
        this.ZAR = zAR;
    }

    public Double getEUR() {
        return EUR;
    }

    public void setEUR(Double EUR) {
        this.EUR = EUR;
    }

    public Map<String, Double> getRates(){
        Map<String, Double> rates = new HashMap<>();
        rates.put("AUD",AUD);
        rates.put("BGN",BGN);
        rates.put("BRL",BRL);
        rates.put("CAD",CAD);
        rates.put("EUR",EUR);
        rates.put("CHF",CHF);
        rates.put("CNY",CNY);
        rates.put("CZK",CZK);
        rates.put("DKK",DKK);
        rates.put("GBP",GBP);
        rates.put("HKD",HKD);
        rates.put("HRK",HRK);
        rates.put("HUF",HUF);
        rates.put("IDR",IDR);
        rates.put("ILS",ILS);
        rates.put("INR",INR);
        rates.put("JPY",JPY);
        rates.put("KRW",KRW);
        rates.put("MXN",MXN);
        rates.put("MYR",MYR);
        rates.put("NOK",NOK);
        rates.put("NZD",NZD);
        rates.put("PHP",PHP);
        rates.put("PLN",PLN);
        rates.put("RON",RON);
        rates.put("RUB",RUB);
        rates.put("SEK",SEK);
        rates.put("SGD",SGD);
        rates.put("THB",THB);
        rates.put("TRY",TRY);
        rates.put("USD",USD);
        rates.put("ZAR",ZAR);
        return rates;
    }

}