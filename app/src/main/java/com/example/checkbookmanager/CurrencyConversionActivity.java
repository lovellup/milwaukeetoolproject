package com.example.checkbookmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CurrencyConversionActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private String item = "USD";
    private Map<String, Double> rates;
    private List<String> currencies;
    private Account account;
    private ArrayAdapter<String> dataAdapter;
    private Spinner spinner;
    private CurrencyConversionActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_conversion);
        Intent intent = getIntent();
        activity = this;
        account = (Account) intent.getSerializableExtra("account");
        rates = new HashMap<>();
        getCurrencies(account.getCurrency());
        spinner = (Spinner) findViewById(R.id.spinner);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Creating adapter for spinner


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        item = parent.getItemAtPosition(position).toString();


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public List<String> getCurrencies(String base) {
        String BASE_URL = FixerIOService.BASE_URL;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FixerIOService service = retrofit.create(FixerIOService.class);
        Call<Currency> call = service.getCurrency(base);
        call.enqueue((new Callback<Currency>() {
            @Override
            public void onResponse(Call<Currency> call, Response<Currency> response) {
                Currency currency = response.body();
                Rate rate = currency.getRates();
                rates = rate.getRates();
                currencies = new LinkedList<>();
                for (Map.Entry<String, Double> entry : rates.entrySet()) {
                    currencies.add(entry.getKey());
                }
                dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, currencies);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spinner.setAdapter(dataAdapter);
            }

            @Override
            public void onFailure(Call<Currency> call, Throwable t) {
                //
            }
        }));
        return currencies;
    }

    public void onConvert(View view) {
        account.setCurrency(item, rates.get(item));
        Intent data = new Intent(this, AccountActivity.class);
        data.putExtra("account", account);
        setResult(RESULT_OK, data);
        finish();
    }

    public Map<String, Double> getRates(){
        return rates;
    }

    public List<String> getCurrencies(){
        return currencies;
    }
}
