package com.example.checkbookmanager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class ErrorDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("You haven't filled in all fields correctly")
                .setPositiveButton("OK", (dialog, id) -> {
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}