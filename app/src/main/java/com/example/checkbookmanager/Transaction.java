package com.example.checkbookmanager;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by lovelli on 2/7/2017.
 */
@Table(name = "Transactions")
public class Transaction extends Model implements Serializable {
    @Column(name = "debit")
    private boolean debit;
    @Column(name = "amount")
    private double amount;
    @Column(name = "sourceFrom")
    private String sourceFrom;
    @Column(name = "dateMade")
    private Calendar dateMade;
    @Column(name = "notes")
    private String notes;
    @Column(name = "transationID")
    private int transactionID;
    private static int highestID = 0;
    @Column(name = "holdingAccountName")
    private String holdingAccountName;

    public int getTransactionId() {
        return transactionID;
    }

    public static int getHighestID() {
        return highestID;
    }

    public Transaction(){
        super();
        debit = false;
        amount = 0.0;
        sourceFrom = "";
        dateMade = new GregorianCalendar();
        notes = "";
        highestID++;
        transactionID = highestID;
        holdingAccountName = "";
    }

    public Transaction(boolean debit, double amount, String sourceFrom, Calendar dateMade, String notes) {
        super();
        this.debit = debit;
        this.amount = amount;
        this.sourceFrom = sourceFrom;
        this.dateMade = dateMade;
        this.notes = notes;
        highestID++;
        this.transactionID = highestID;
    }

    public Transaction(boolean debit, double amount, String sourceFrom, Calendar dateMade, int transactionID, String notes) {
        this.debit = debit;
        this.amount = amount;
        this.sourceFrom = sourceFrom;
        this.dateMade = dateMade;
        this.notes = notes;
        if(transactionID >highestID){
            highestID= transactionID;
        }
        this.transactionID = transactionID;
    }

    public static double calculateDifference(Transaction original, Transaction newOne){
        double realAmountO = Transaction.calculateRealAmount(original);
        double realAmountN = Transaction.calculateRealAmount(newOne);
        return realAmountN-realAmountO;
    }

    public static double calculateRealAmount(Transaction t){
        double realAmount = t.getAmount();
        if(!t.isDebit()){
            realAmount=realAmount*(-1);
        }
        return realAmount;
    }

    public String getHoldingAccountName() {
        return holdingAccountName;
    }

    public void setHoldingAccountName(String holdingAccountName) {
        this.holdingAccountName = holdingAccountName;
    }

    public boolean isDebit() {
        return debit;
    }

    public void setDebit(boolean debit) {
        this.debit = debit;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getSourceFrom() {
        return sourceFrom;
    }

    public void setSourceFrom(String sourceFrom) {
        this.sourceFrom = sourceFrom;
    }

    public Calendar getDateMade() {
        return dateMade;
    }

    public void setDateMade(Calendar dateMade) {
        this.dateMade = dateMade;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }
}
