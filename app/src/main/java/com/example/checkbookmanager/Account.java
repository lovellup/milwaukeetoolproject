package com.example.checkbookmanager;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lovelli on 2/7/2017.
 */
@Table(name = "Account")
public class Account extends Model implements Serializable {
    @Column(name = "name")
    public String accountName;
    @Column(name = "bank")
    public String bank;
    @Column(name = "accountNumber")
    public long accountNumber;
    @Column(name = "routingNumber")
    public long routingNumber;
    @Column(name = "balance")
    public double balance;
    private List<Transaction> transactionList;
    @Column(name = "currency")
    public String currency;

    public Account(){
        accountName="";
        bank = "";
        accountNumber = 0L;
        routingNumber = 0L;
        balance = 0.0;
        transactionList = new LinkedList<>();
        currency = "USD";

    }

    public String getCurrency() {
        return currency;
    }


    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(long routingNumber) {
        this.routingNumber = routingNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Account(String accountName, String bank, long accountNumber, long routingNumber, double balance) {
        this.accountName = accountName;
        this.bank = bank;
        this.accountNumber = accountNumber;
        this.routingNumber = routingNumber;
        this.balance = balance;
        this.currency = "USD";
        this.transactionList = new LinkedList<>();

    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public void addTransaction(Transaction transaction) {
        for (Transaction t : transactionList) {
            if (t.getTransactionId() == transaction.getTransactionId()) {
                double difference = Transaction.calculateDifference(t, transaction);
                transaction.setHoldingAccountName(this.accountName);
                transactionList.set(transactionList.indexOf(t), transaction);
                balance += difference;
                return;
            }
        }
        transactionList.add(transaction);
        transaction.setHoldingAccountName(this.accountName);
        balance += Transaction.calculateRealAmount(transaction);
    }


    public void setCurrency(String currency, Double rate) {
        this.currency = currency;
        balance = balance * rate;
        for (Transaction t : transactionList) {
            t.setAmount(t.getAmount() * rate);
            transactionList.set(transactionList.indexOf(t), t);
        }
    }


}
