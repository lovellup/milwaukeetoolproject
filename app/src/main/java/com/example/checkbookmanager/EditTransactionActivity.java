package com.example.checkbookmanager;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.example.checkbookmanager.databinding.ActivityEditTransactionBinding;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class EditTransactionActivity extends AppCompatActivity {
    private boolean debit = true;
    private int id;
    private ActivityEditTransactionBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_transaction);
        Intent intent = getIntent();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_transaction);
        id = intent.getIntExtra("transID",0);
        Switch credit = (Switch) findViewById(R.id.credit);
        credit.setOnCheckedChangeListener((buttonView, isChecked) -> toggleCredit(isChecked));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(id!=0){
            Transaction transaction =(Transaction) getIntent().getSerializableExtra("transaction");
            binding.setTransaction(transaction);
            DatePicker dateMade = (DatePicker) findViewById(R.id.dateMade);
            dateMade.updateDate(transaction.getDateMade().get(Calendar.YEAR),transaction.getDateMade().get(Calendar.MONTH),transaction.getDateMade().get(Calendar.DAY_OF_MONTH));
            Switch sw = (Switch)findViewById(R.id.credit);
            sw.setChecked(debit);
        }
    }

    public void saveTransaction(View view) {
        Intent data = new Intent(getBaseContext(),AccountActivity.class);
        if (errorChecking()) {
            EditText amountEditText = (EditText)findViewById(R.id.trAmount);
            EditText destSourceValue = (EditText) findViewById(R.id.destSourceValue);
            double amount = Double.parseDouble(amountEditText.getText().toString());
            String destSource = destSourceValue.getText().toString();
            DatePicker datePicker = (DatePicker)findViewById(R.id.dateMade);
            Calendar calendar = new GregorianCalendar(datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth());
            EditText note = (EditText)findViewById(R.id.trNotes);
            String notes = note.getText().toString();
            Transaction transaction;
            if(id==0){
                transaction = new Transaction(this.debit,amount,destSource,calendar,notes);
            } else {
                transaction = new Transaction(this.debit,amount,destSource,calendar,id,notes);
            }
            data.putExtra("transaction", transaction);
            setResult(Activity.RESULT_OK,data);
            finish();
        } else {
            ErrorDialog errorDialog = new ErrorDialog();
            errorDialog.show(this.getFragmentManager(), "");
        }
    }

    private boolean errorChecking() {
        EditText amountEditText = (EditText)findViewById(R.id.trAmount);
        EditText destSourceValue = (EditText) findViewById(R.id.destSourceValue);
        if (amountEditText.getText().toString().equals("") || (destSourceValue.getText().toString().equals(""))) {
            return false;
        }
        return true;
    }

    public void toggleCredit(boolean debit) {
        this.debit=debit;
        TextView trAmountText =(TextView) findViewById(R.id.trAmountText);
        EditText trAmount = (EditText) findViewById(R.id.trAmount);
        TextView destSourceText = (TextView) findViewById(R.id.destSourceText);
        if (!debit){
            trAmount.setTextColor(Color.GREEN);
            trAmountText.setTextColor(Color.GREEN);
            destSourceText.setText("Source");
        } else {
            trAmount.setTextColor(Color.RED);
            trAmountText.setTextColor(Color.RED);
            destSourceText.setText("Paid To");
        }

    }


}
