package com.example.checkbookmanager;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by lovelli on 2/8/2017.
 */

public interface FixerIOService {
    public final static String BASE_URL = "http://api.fixer.io/";

    @GET("latest")
    Call<Currency> getCurrency(@Query("base") String base);

}
