package com.example.checkbookmanager;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import java.util.LinkedList;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity {
    public static List<Account> accountList;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SQLiteDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.d("myTag", "This is my message");
        setContentView(R.layout.activity_welcome);
        ActiveAndroid.initialize(getApplication());

        //accountList = new Select().from(Account.class).execute();
        accountList = new LinkedList<>();
        recyclerView = (RecyclerView) findViewById(R.id.my_account_recycler_view);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
    }

    public void addAccount(View view) {
        Intent intent = new Intent(this, EditAccountActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Account account = (Account) data.getSerializableExtra("account");
        accountList.add(account);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (accountList.size() > 0) {
            TextView noAccountWelcome = (TextView) findViewById(R.id.noAccount);
            noAccountWelcome.setText("");
            AccountCardAdapter accountCardAdapter = new AccountCardAdapter(accountList);
            recyclerView.setAdapter(accountCardAdapter);
            recyclerView.setClickable(true);
        }
    }


}

