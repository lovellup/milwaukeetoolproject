package com.example.checkbookmanager;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class AccountCardAdapter extends RecyclerView.Adapter<AccountCardAdapter.ViewHolder> {
    private List<Account> mDataset;



    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        // each data item is just a string in this case
        private TextView mAccountName;
        private TextView mBalance;
        private CardView mCard;
        public ViewHolder(View v) {
            super(v);
            mCard=(CardView) v.findViewById(R.id.card_view);
            mAccountName = (TextView) v.findViewById(R.id.accountName);
            mBalance = (TextView) v.findViewById(R.id.accountBalance);
            mCard.setClickable(true);
            mCard.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            Intent accountIntent = new Intent(context, AccountActivity.class);
            accountIntent.putExtra("accName", mAccountName.getText().toString());
            context.startActivity(accountIntent);
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AccountCardAdapter
    (List<Account> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AccountCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_view_account_row, parent, false);
//        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(inflatedView);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mAccountName.setText(mDataset.get(position).getAccountName());
        holder.mBalance.setText(mDataset.get(position).getBalance()+"");

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}