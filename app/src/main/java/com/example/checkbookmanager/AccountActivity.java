package com.example.checkbookmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;

public class AccountActivity extends AppCompatActivity {
    Account account;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        Intent intent = getIntent();
        for(Account acc: WelcomeActivity.accountList){
            if(acc.getAccountName().equals(intent.getStringExtra("accName"))){
                account=acc;
            }
        }
        recyclerView = (RecyclerView) findViewById(R.id.my_transaction_card_view);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView nameText = (TextView)findViewById(R.id.accName);
        nameText.setText(account.getAccountName());
        TextView bankText = (TextView)findViewById(R.id.accBank);
        bankText.setText(account.getBank());
        TextView accountNumberText = (TextView)findViewById(R.id.accAccountNumber);
        accountNumberText.setText(account.getAccountNumber()+"");
        TextView routingNumberText = (TextView)findViewById(R.id.accRoutingNumber);
        routingNumberText.setText(account.getRoutingNumber()+"");
        TextView balanceText = (TextView)findViewById(R.id.accBalance);
        balanceText.setText(account.getBalance()+"");
        Intent intent = getIntent();
        if (intent.hasExtra("transaction")){
            onActivityResult(1,1,intent);
        }
        if(account.getTransactionList().size()>0) {
            TransactionCardAdapter transactionCardAdapter = new TransactionCardAdapter(account.getTransactionList(),this);
            recyclerView.setAdapter(transactionCardAdapter);
            recyclerView.setClickable(true);
        }
    }

    public void addTransaction(View view){
        Intent intent = new Intent(this, EditTransactionActivity.class);
        startActivityForResult(intent,1);
    }

    public void addTransaction(View view, Intent intent){
        Intent intent1 = new Intent(this,EditTransactionActivity.class);
        intent1.putExtras(intent);
        startActivityForResult(intent1,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1) {
            Transaction transaction = (Transaction) data.getSerializableExtra("transaction");
            account.addTransaction(transaction);
            for(Account acc: WelcomeActivity.accountList){
                if(acc.getAccountName().equals(account.getAccountName())){
                    WelcomeActivity.accountList.set(WelcomeActivity.accountList.indexOf(acc),account);
                }
            }
            onResume();
        } else if (requestCode ==2) {
            account = (Account) data.getSerializableExtra("account");
            for(Account acc: WelcomeActivity.accountList){
                if(acc.getAccountName().equals(account.getAccountName())){
                    WelcomeActivity.accountList.set(WelcomeActivity.accountList.indexOf(acc),account);
                }
            }
            onResume();
        }
    }

    public void onConvert(View view){
        Intent intent = new Intent(this, CurrencyConversionActivity.class);
        intent.putExtra("account", account);
        startActivityForResult(intent,2);
    }

    public void onSave(View view){
//        ActiveAndroid.beginTransaction();
//        try {
//            for (Transaction t : account.getTransactionList()) {
//                t.save();
//            }
//            account.save();
//            ActiveAndroid.setTransactionSuccessful();
//        } finally {
//            ActiveAndroid.endTransaction();
//        }
    }
}
